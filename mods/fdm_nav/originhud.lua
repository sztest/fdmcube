-- LUALOCALS < ---------------------------------------------------------
local minetest, vector
    = minetest, vector
-- LUALOCALS > ---------------------------------------------------------

minetest.register_on_joinplayer(function(player)
		player:hud_add({
				[minetest.features.hud_def_type_field
				and "type" or "hud_elem_type"] = "image_waypoint",
				world_pos = vector.new(0, 9.5, 0),
				text = "fdm_nav_origin.png^[opacity:128",
				scale = {x = 1, y = 1}
			})
	end)
