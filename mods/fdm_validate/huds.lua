-- LUALOCALS < ---------------------------------------------------------
local fdm_api, ipairs, minetest, next, pairs, vector
    = fdm_api, ipairs, minetest, next, pairs, vector
-- LUALOCALS > ---------------------------------------------------------

local hudrange = 80

local modname = minetest.get_current_modname()
local hash = minetest.hash_node_position
local posstr = minetest.pos_to_string

-- Pre-index all "bad" node names so we can check them quicker than
-- with minetest.get_item_group.

local badnodes = {}
minetest.after(0, function()
		for k, v in pairs(minetest.registered_nodes) do
			if v.groups and v.groups[modname .. "_bad"] then
				badnodes[k] = true
			end
		end
	end)

------------------------------------------------------------------------
-- Detect Bad Nodes

-- Running map of all bad nodes known, in hash(pos) => pos form.
local found = {}

local function addfound(pos) found[hash(pos)] = pos end

-- Search for all known new bad nodes via LBM, ABM and node update
-- hooks. This detects new ones quickly; we filter out old removed
-- ones later in our globalstep loop.

minetest.register_lbm({
		name = modname .. ":detect",
		run_at_every_load = true,
		nodenames = {"group:" .. modname .. "_bad"},
		action = addfound
	})
minetest.register_abm({
		nodenames = {"group:" .. modname .. "_bad"},
		interval = 1,
		chance = 1,
		action = addfound
	})
fdm_api.register_on_nodeupdate(function(pos, node)
		if badnodes[node.name] then return addfound(pos) end
	end)

------------------------------------------------------------------------
-- Player HUDs

-- Sync up player HUDs so that there's always one image_waypoint
-- for each problem location, adding/removing HUDs as needed.

local huds = {}
minetest.register_on_leaveplayer(function(player) huds[player:get_player_name()] = nil end)

local function checkplayer(player, locations)
	local pname = player:get_player_name()

	local ppos = player:get_pos()
	ppos.y = ppos.y + player:get_properties().eye_height

	local phuds = huds[pname] or {}
	local newhuds = {}
	for k, v in pairs(locations) do
		if vector.distance(ppos, v) <= hudrange then
			local old = phuds[k]
			if old then
				newhuds[k] = old
			else
				newhuds[k] = player:hud_add({
						[minetest.features.hud_def_type_field
						and "type" or "hud_elem_type"] = "image_waypoint",
						world_pos = v,
						text = "fdm_material_basic.png^[invert:rgb"
						.. "^[resize:32x32^[mask:fdm_validate_hud.png"
						.. "^[opacity:192",
						scale = {x = 1, y = 1}
					})
			end
		end
	end
	for k, v in pairs(phuds) do
		if not newhuds[k] then
			player:hud_remove(v)
		end
	end

	huds[pname] = newhuds
end

------------------------------------------------------------------------
-- Filtering/Maintenance

-- Floodfill scan to find all nodes in a face-connected contiguous
-- region within the "found" set.

local function scanflood(startk, startv, match, seen)
	if seen[startk] or not found[startk] then return end
	match[startk] = startv
	seen[startk] = true
	local function rel(dx, dy, dz)
		local v = vector.add(startv, vector.new(dx, dy, dz))
		local k = hash(v)
		return scanflood(k, v, match, seen)
	end
	rel(-1, 0, 0)
	rel(1, 0, 0)
	rel(0, -1, 0)
	rel(0, 1, 0)
	rel(0, 0, -1)
	return rel(0, 0, 1)
end

minetest.register_globalstep(function()
		-- Remove nodes that are no longer "bad".
		local filtered = {}
		for k, v in pairs(found) do
			local n = minetest.get_node(v)
			if badnodes[n.name] then
				filtered[k] = v
			end
		end
		found = filtered

		-- Find the minimum set of centroids of face-connected regions
		-- within the found set, so we can show only one HUD for each
		-- region instead of one for each node (which can be overwhelming
		-- if the region has a lot of nodes).
		local regions = {}
		local seen = {}
		for k, v in pairs(found) do
			if not seen[k] then
				local match = {}
				scanflood(k, v, match, seen)
				if next(match) then
					local agg = vector.new(0, 0, 0)
					local qty = 0
					for _, m in pairs(match) do
						agg = vector.add(agg, m)
						qty = qty + 1
					end
					agg = vector.multiply(agg, 1 / qty)
					regions[posstr(agg)] = agg
				end
			end
		end

		-- Check and update all player HUDs.
		for _, player in ipairs(minetest.get_connected_players()) do
			checkplayer(player, regions)
		end
	end)
