-- LUALOCALS < ---------------------------------------------------------
local fdm_api, minetest, rawget, rawset, vector
    = fdm_api, minetest, rawget, rawset, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local api = rawget(_G, modname) or {}
rawset(_G, modname, api)

------------------------------------------------------------------------
-- Immediate Check Utility

-- Perform an immediate check on a node for validation, and apply
-- the good/bad alternative change if needed.

-- node parameter is optional; if provided, it's trusted.

function api.fdm_validate_check(pos, node)
	node = node or minetest.get_node(pos)
	local def = minetest.registered_nodes[node.name]
	local val = def and def.fdm_validate
	if not val then return end
	local valid = val(pos, node)
	if valid and def.fdm_alternative_good then
		node.name = def.fdm_alternative_good
		return minetest.set_node(pos, node)
	elseif (not valid) and def.fdm_alternative_bad then
		node.name = def.fdm_alternative_bad
		return minetest.set_node(pos, node)
	end
end

------------------------------------------------------------------------
-- Immediate Check on Node Change

-- If a node is updated, check it immediately, as well as all of its
-- immediate neighbors (recursively if necessary).

fdm_api.register_on_nodeupdate(function(pos, node)
		local check = api.fdm_validate_check
		check(pos, node)
		for dx = -1, 1 do
			for dy = -1, 1 do
				for dz = -1, 1 do
					check(vector.add(pos, vector.new(dx, dy, dz)))
				end
			end
		end
	end)

------------------------------------------------------------------------
-- Background Validation

-- LBM to recheck immediately upon reloading areas.

minetest.register_lbm({
		name = modname .. ":check",
		run_at_every_load = true,
		nodenames = {"group:fdm_validate"},
		action = function(...) return api.fdm_validate_check(...) end
	})

-- ABM to "patrol" for nodes that need to be checked in background
-- automatically, to ensure nothing can slip past our hooks indefinitely
-- while loaded. Even if this turns out to be "expensive" and exceeds
-- the ABM budget, as it's the only ABM we anticipate needing in the
-- entire game mode, we should be fine if some mapblocks are cut off
-- and deferred until some random future check.

minetest.register_abm({
		nodenames = {"group:fdm_validate"},
		interval = 1,
		chance = 1,
		action = function(...) return api.fdm_validate_check(...) end
	})
