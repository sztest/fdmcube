-- LUALOCALS < ---------------------------------------------------------
local minetest, rawget, rawset
    = minetest, rawget, rawset
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local api = rawget(_G, modname) or {}
rawset(_G, modname, api)

-- Print bed can't be invalid, but always provides support.

api.register_validation("fdm_material:bed", nil, function() return true end)
