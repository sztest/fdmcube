*Support/Discussion on [Warr1024's Small Projects Discord](https://discord.gg/KMbCjAHBTs)*

## Printability Validation

Suspected printability issues (bad bridges or overhangs, things that would require extra support material) will be automatically detected and highlighted in **crimson**. Minimizing these areas will lead to prints that require less support material and come out with better quality.

Note that the printability validation is "conservative" in some cases and does not allow things that would actually work fine, like bridges made out of wedge shapes, while a full slicer may still be able to find a strategy to print these. This is not necessarily an error.

## How to Use

- Install the game, along with the recommended mods:
	- [WorldEdit](/packages/sfan5/worldedit/)
	- [Meshport](/packages/random_geek/meshport/)
- Start a world.
	- Host a server if you want to build collaboratively.
	- It's recommended to add `fly,fast,noclip` to default privileges,
	  and/or grant them to each person who is helping build.
	- Revoking `interact` privs for spectators should also work.
- Build things using the provided materials.
	- You can choose any scale you want for your models (within the
	  capabilities of the engine and your computer).
	- Nodes that turn **crimson** are warning you about printability issues
	  where your slicer will need to generate extra support (costs more time,
	  effort, materials to print) or cause other problems elsewhere (sagging
	  bridges or overhangs) so try to minimize them.
- Export your model.
	- Use WorldEdit to move your model up off the printbed (see Known Issues).
	- Select you model, and at least 1 node of open air on each face of it,
	  using meshport, and export it.
- Slice and print.
	- Load the exported OBJ file into your slicer, scale it to the size you
	  want, load your settings, and send it off to your printer.

## Known Issues

### Meshport Bottom Surfaces

If exporting a model that's built directly on the print bed, Meshport
will not include the bottom surfaces in the model, causing it to break
badly in some slicers.  Use WorldEdit to move the entire model up off
the bed before exporting, and include 1 node of airspace on each of
the 6 faces around the model to ensure it's fully closed.

When using PrusaSlicer, if you don't correct for this and leave the bottom
faces of the model unclosed, as long as the model is oriented correctly,
it seems like it doesn't usually actually create problems with slicing,
even though it shows visible warnings.