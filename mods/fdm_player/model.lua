-- LUALOCALS < ---------------------------------------------------------
local minetest
    = minetest
-- LUALOCALS > ---------------------------------------------------------

minetest.register_on_joinplayer(function(player)
		player:set_properties({
				collisionbox = {-0.3, -0.5, -0.3, 0.3, 0.45, 0.3},
				eye_height = 0.3,
				visual_size = {x = 1, y = 1, z = 1},
				visual = "cube",
				textures = {
					"fdm_player_model_top.png",
					"fdm_player_model_bot.png",
					"fdm_player_model_left.png",
					"fdm_player_model_right.png",
					"fdm_player_model_front.png",
					"fdm_player_model_back.png",
				},
				pointable = false,
				backface_culling = true,
			})
	end)
