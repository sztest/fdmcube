-- LUALOCALS < ---------------------------------------------------------
local fdm_api, minetest, rawget, rawset, vector
    = fdm_api, minetest, rawget, rawset, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local api = rawget(_G, modname) or {}
rawset(_G, modname, api)

api.register_validation("fdm_material:outer",
	function(pos, node)
		local facedir = fdm_api.facedirs[node.param2]

		-- XXX: SIMPLIFY: disallow "wall" placement (similar
		-- to ramp case).
		if facedir.b.y == 0 then return end

		-- Corners with a flat bottom only need bottom support.
		-- XXX: SIMPLIFY: don't attempt to support bridging with
		-- non-full-cubes for now.
		-- Floor-placed corners will always have their "bottom" vector
		-- downwards, we don't need to worry about cases where the "back"
		-- vector is the downwards one.
		if facedir.b == fdm_api.dirs.d then
			return api.source_check(vector.add(pos, facedir.b), nil, pos)
		end

		-- For ceiling-facing, it needs to receive at least "partial/corner"
		-- support on both faces.
		local fullr, partr = api.source_check(vector.add(pos, facedir.r), nil, pos)
		local fullk, partk = api.source_check(vector.add(pos, facedir.k), nil, pos)
		if not (fullr or partr and vector.equals(partr, vector.add(facedir.b, facedir.k)))
		and (fullk or partk and vector.equals(partk, vector.add(facedir.b, facedir.r)))
		then return end
		-- Ceiling placement: the nodes we're reciving support from must
		-- be supported fully from below to have something for the bottom
		-- lip of our node to build from.
		local below = vector.add(pos, fdm_api.dirs.d)
		local corner = vector.add(facedir.k, facedir.r)
		if (not api.source_check(vector.add(below, corner),
				nil, vector.add(pos, corner))) then return end
		return true
	end,
	function(pos, node, from)
		-- Ramps only provide upward support if they have a full
		-- flat face in the direction in question.
		local fromdir = vector.subtract(from, pos)
		local facedir = fdm_api.facedirs[node.param2]
		if vector.equals(fromdir, facedir.b) then return true end
		if vector.equals(fromdir, facedir.r) then
			return false, vector.add(facedir.b, facedir.k)
		end
		if vector.equals(fromdir, facedir.k) then
			return false, vector.add(facedir.b, facedir.r)
		end
	end)
