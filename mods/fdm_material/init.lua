-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, vector
    = minetest, pairs, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

------------------------------------------------------------------------
-- Print Bed Node

-- This is effectively a "bedrock" material that will be used by mapgen
-- to provide the print bed substrate.

local brname = modname .. ":bed"
minetest.register_node(brname, {
		tiles = {"fdm_material_bed.png^fdm_material_frame.png"}
	})

minetest.register_alias("mapgen_stone", brname)
minetest.register_alias("mapgen_water_source", brname)
minetest.register_alias("mapgen_river_water_source", brname)

------------------------------------------------------------------------
-- Basic Material

-- Helper for registering player-usable material nodes that can be
-- placed, destroyed, and rotated if applicable.

local vechalf = vector.new(0.5, 0.5, 0.5)
local function reg(subname, def)
	-- Players should always have exactly one in their inventory,
	-- disallow getting more or dropping any.
	def.stack_max = 1
	def.on_drop = function() end

	-- Override on_place to do the "creative mode" thing and never
	-- consume quantity, regardless of the state of configuration.
	-- Facedir nodes automatically rotate upon placement.
	def.on_place = def.paramtype2 == "facedir"
	and function(stack, placer, pt)
		-- XXX: SIMPLIFY: disallow any "wall" oriented
		-- nodes and only allow floor or ceiling rotations
		-- (8 directions possible instead of the default 12)
		local invert_wall = pt.above.y == pt.under.y or false
		minetest.rotate_and_place(stack, placer, pt, true,
			{invert_wall = invert_wall}, true)
		-- return nil to not modify stack
	end
	or function(...)
		minetest.item_place(...)
		-- return nil to not modify stack
	end

	-- Respond to "zapper" by destroying the node (see fdm_player zapper)
	def.on_fdm_zapper = function(pos, node)
		minetest.add_particlespawner({
				time = 0.02,
				amount = 25,
				exptime = {min = 0.25, max = 1, bias = 1},
				size = {
					min = 0.2,
					max = 0.5,
					bias = 1
				},
				collisiondetection = false,
				node = {name = node.name},
				pos = {
					min = vector.subtract(pos, vechalf),
					max = vector.add(pos, vechalf),
				},
				attract = {
					kind = "point",
					strength = {
						min = -1,
						max = -5,
						bias = 1
					},
					origin = pos
				}
			})
		minetest.remove_node(pos)
	end

	-- Allow light to propagate through all player-created
	-- structures, effectively enabling "fullbright" for the entire map.
	def.paramtype = "light"
	def.sunlight_propagates = true

	-- Create 2 versions of each material node:
	-- - a "good" one with normal appearance indicating no issues
	-- - a "bad" one with inverted color that indicates some structural problem
	-- These nodes are linked to one another via fdm_alternative_* fields
	-- to enable validation system to swap them.

	-- All materials require interact privs to use.
	def.fdm_inv_priv = "interact"

	local mainname = modname .. ":" .. subname
	local badname = modname .. ":" .. subname .. "_bad"

	local maindef = {}
	for k, v in pairs(def) do maindef[k] = v end
	maindef.tiles = {"fdm_material_basic.png^fdm_material_frame.png"}
	maindef.fdm_alternative_bad = badname
	minetest.register_node(mainname, maindef)

	local baddef = {}
	for k, v in pairs(def) do baddef[k] = v end
	baddef.fdm_inv_slot = nil
	baddef.fdm_alternative_good = mainname
	baddef.tiles = {"fdm_material_basic.png^[invert:rgb^fdm_material_frame.png"}
	minetest.register_node(badname, baddef)
end

-- Register the one basic solid cube node.
reg("basic", {fdm_inv_slot = 1})

------------------------------------------------------------------------
-- Ramp/Corner Mesh Nodes

-- Helper function to register mesh nodes with special logic
local function regmesh(name, slot, boxfunc)
	-- Number of steps used to define collision box. More steps creates
	-- more smooth movement, but may require more computation work to
	-- calculate collisions.
	local steps = 16

	-- Generate the collision box using parameterized logic.
	local boxes = {}
	for i = 1, steps do
		local t = {boxfunc(i / steps)}
		for j = 1, #t, 4 do
			boxes[#boxes + 1] = {t[j], -0.5 + (i - 1) / steps, t[j + 1],
				t[j + 2], -0.5 + i / steps, t[j + 3]}
		end
	end

	-- Register node via helper.
	return reg(name, {
			drawtype = "mesh",
			collision_box = {type = "fixed", fixed = boxes},
			mesh = "fdm_material_" .. name .. ".obj",
			fdm_inv_slot = slot,
			paramtype2 = "facedir",
			tiles = {"fdm_material_basic.png^fdm_material_frame.png"}
		})
end

-- Simple "edge" ramp used for 45 degree overhangs and chamfers.
regmesh("ramp", 2, function(i) return -0.5, -0.5 + i, 0.5, 0.5 end)

-- Inner and outer corners where edge ramps meet.
regmesh("inner", 3, function(i) return -0.5, -0.5 + i, 0.5, 0.5, -0.5 + i, -0.5, 0.5, 0.5 end)
regmesh("outer", 4, function(i) return -0.5 + i, -0.5 + i, 0.5, 0.5 end)
