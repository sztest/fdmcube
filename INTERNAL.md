# FDM Cube - Internal Documentation

## Internal APIs

### Node Zapper

When the player uses left-click (punch/dig) on a node, in addition to all
standard hooks and callbacks, an attempt will be made to "zap" the node.

- Like punching but unlike digging, zapping happens instantly on the first
  node pointed at.
- Like digging but unlike punching, zapping does NOT instantly happen on
  the next node behind the zapped one, if the zapped one is removed; there
  is a 0.25s cooldown time to give the player a chance to release the
  button and not zap the node behind.

Nodes that are zapped will receive the `def.on_fdm_zapper(pos, node, player)`
callback.  If they are destructible then they are expected to remove
themselves, along with any other associated special effects.

### Structural Support

The `fdm_validate.register_validation(nodename, validation_func, source_func)`
API can be used to register material validation rules.

- The `validation_func(pos, node)` function does a validation check, and
  returns a truthy value if the node is properly supported.
- The `source_func(pos, node, from)` function checks to see if this node
  is willing to provide support for the node at position `from` and returns:
	- truthy *(TBD: may expand to support partial-face support)* if this node
	  is willing to provide that support.
	- false, and then a direction, if the node is only able to provide
	  "half/corner" support.  The vector will be in the direction of the
	  bias of the triangle where the support will be, and exactly 2 of
	  its xyz components will have magnitude 1 and 1 will be magnitude 0.
	- nil/falsy only, if it offers no support.
- The node itself should be one of a pair of nodes, one representing a
  "good" state and one representing a "bad" state, each one linked to the
  other by having a `fdm_alternative_<good/bad>` field in the node
  definition that contains the fully qualified name of the alternative.

Nodes will be automatically validated under various circumstances (any change,
periodic/random checks) and replaced with the good/bad alternative based
on the validation results, which is used to display potential problems
to the user.

### Code Issues

All code issues should be tagged with an `XXX:` comment block.  Further
comment codes like `TODO:` or `FIXME:` can be used after the XXX: to
allow narrowing search criteria to specific types of issues.

It should be possible to find every known issue that's documented in
code by doing a case-sensitive search for `XXX` across the project.

## TODO

- Navigation features
	- Adding labels/waypoints
	- Teleporting to labels/waypoints
		- Search by name, teleport if only one match
	- Maybe put navigation in inventory formspec?
		- Chat commands as backup?
- Exporting
	- Meshport is LGPL so it will need to be cleanroomed
	- Need to ignore the print bed
	- Better interface instead of just cubic corners
		- Recursively fill object pointed at
- Ability to mark validation overrides
	- Mark a problem as having been manually checked and acceptable
		- Actually printable things the validator can't handle (diagnonal bridges)
		- Things where use of supports is deemed acceptable
	- Replace with a yellow version of node, hide any warning HUDs
	- Invalidate if node or any neighbor changes
- Importing
	- Load MT schematics, WE files, mapping only rough solidity
	- Import meshes a la voxelizer (cleanroom) or voxeload
	- Exporter mod that runs in other game and allows export to fdmcube usable format
- Built-in player guide in "inventory" screen
	- "About" information
	- Explanation of FDM printing guidelines, bridging, overhangs
- Special nodes like sacrificial bridges
- Area validation overrides
	- e.g. within a certain area or radius, override max bridge length
- Special tools
	- Make a double-scale copy of an existing model