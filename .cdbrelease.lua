-- LUALOCALS < ---------------------------------------------------------
local math, tonumber
    = math, tonumber
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

local stamp = tonumber("$Format:%at$")
if not stamp then return end
stamp = math_floor((stamp - 1540612800) / 60)
stamp = ("00000000" .. stamp):sub(-8)

-- luacheck: push
-- luacheck: globals config readtext readbinary

readtext = readtext or function() end
readbinary = readbinary or function() end

return {
	user = "Warr1024",
	pkg = "fdmcube",
	version = stamp .. "-$Format:%h$",
	type = "game",
	dev_state = "ACTIVELY_DEVELOPED",
	title = "FDM Cube",
	short_description = "Design models for FDM 3D printing",
	tags = {"creative", "building", "education", "oneofakind__original"},
	license = "MIT",
	media_license = "MIT",
	repo = "https://gitlab.com/sztest/fdmcube",
	long_description = readtext('.cdb-readme.md'),
	screenshots = {readbinary('.cdb-screenshot.webp')}
}

-- luacheck: pop
