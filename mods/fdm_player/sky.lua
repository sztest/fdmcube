-- LUALOCALS < ---------------------------------------------------------
local minetest
    = minetest
-- LUALOCALS > ---------------------------------------------------------

minetest.register_on_joinplayer(function(player)
		-- Always full daylight
		player:override_day_night_ratio(1)

		-- Simple skybox
		local s = "^[combine:16x16^[noalpha^[colorize:#808080:255"
		player:set_sky({
				type = "skybox",
				textures = {s, s, s, s, s, s},
				base_color = "#808080"
			})
		player:set_sun({visible = false, sunrise_visible = false})
		player:set_moon({visible = false})
		player:set_stars({visible = false})
		player:set_clouds({density = 0})
	end)
