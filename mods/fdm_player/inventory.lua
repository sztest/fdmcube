-- LUALOCALS < ---------------------------------------------------------
local ItemStack, ipairs, minetest, pairs
    = ItemStack, ipairs, minetest, pairs
-- LUALOCALS > ---------------------------------------------------------

-- Cache filtered list of eligible items, for efficiency.
local filtereditems = {}
minetest.after(0, function()
		for k, v in pairs(minetest.registered_items) do
			if v.fdm_inv_slot then filtereditems[k] = v end
		end
	end)

-- Fully update the player's inventory and UI.
local function updateinv(player)
	-- Find all items we have access to.
	local privs = minetest.get_player_privs(player:get_player_name())
	local items = {}
	for k, v in pairs(filtereditems) do
		if v.fdm_inv_slot and ((not v.fdm_inv_priv)
			or privs[v.fdm_inv_priv]) then
			items[v.fdm_inv_slot] = k
		end
	end
	-- Calculate the length of the hotbar needed.
	local max = 0
	for i in pairs(items) do
		if i > max then max = i end
	end
	-- Reset inventory size and items.
	local inv = player:get_inventory()
	if inv:get_size("main") ~= max then inv:set_size("main", max) end
	for i = 1, max do
		local newstack = ItemStack(items[i])
		local oldstack = inv:get_stack("main", i)
		if newstack:to_string() ~= oldstack:to_string() then
			inv:set_stack("main", i, newstack)
		end
	end
	-- Fix hotbar length/visibility.
	local function hudhotbar(val)
		if player:hud_get_flags().hotbar ~= val then
			player:hud_set_flags({hotbar = val})
		end
	end
	if max == 0 then
		hudhotbar(false)
	else
		hudhotbar(true)
		if player:hud_get_hotbar_itemcount() ~= max then
			player:hud_set_hotbar_itemcount(max)
		end
	end
end

-- Fix hotbar/wielditem visibility if it doesn't match player's privileges

minetest.register_on_joinplayer(function(player)
		-- Initialize hotbar image
		player:hud_set_hotbar_image("[combine:16x16")
		player:hud_set_hotbar_selected_image("fdm_player_select.png")

		-- Disable inventory screen
		player:set_inventory_formspec("")

		-- Remove all lists except main
		local inv = player:get_inventory()
		for k in pairs(inv:get_lists()) do
			if k ~= "main" then inv:set_size(k, 0) end
		end

		updateinv(player)
	end)

-- Players may not alter inventory
minetest.register_allow_player_inventory_action(function() return 0 end)

-- Keep hotbar visibility in sync with privileges
minetest.register_globalstep(function()
		for _, player in ipairs(minetest.get_connected_players()) do
			updateinv(player)
		end
	end)
