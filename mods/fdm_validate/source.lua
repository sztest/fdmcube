-- LUALOCALS < ---------------------------------------------------------
local fdm_api, minetest, rawget, rawset
    = fdm_api, minetest, rawget, rawset
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local api = rawget(_G, modname) or {}
rawset(_G, modname, api)

-- Check if a node is willing to be a source of support in
-- a specific direction. "From" is the position from which
-- the request for support is coming (i.e. the position TO
-- which support would be provided)

-- node parameter is optional; if provided, it's trusted.

function api.source_check(pos, node, from)
	-- A node below cannot request a node above support it
	if from.y < pos.y then return end

	node = node or minetest.get_node(pos)
	local def = minetest.registered_nodes[node.name]
	local src = def and def.fdm_source
	if not src then return end
	return src(pos, node, from)
end

-- Check if a node is capable of supporting an overhang. It is
-- if it's NOT an overhang itself (to prevent circular deps).
function api.overhang_check(pos, node)
	node = node or minetest.get_node(pos)

	-- We assume that non-facedir nodes don't use param2 at all
	-- (and so it will be 0, which has bottom facing down) and
	-- ones that do will be bottom-up if they're overhangs.
	local facedir = fdm_api.facedirs[node.param2]
	return facedir.b.y < 0
end
