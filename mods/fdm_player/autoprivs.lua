-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs
    = minetest, pairs
-- LUALOCALS > ---------------------------------------------------------

-- Override these privs so that SP/admin players receive them
-- automatically. Overriding the default_privs for additional
-- players on a server remains the responsibility of server admins.

for k in pairs({fly = 1, fast = 1, noclip = 1}) do
	local priv = minetest.registered_privileges[k]
	if priv then
		priv.give_to_admin = true
		priv.give_to_singleplayer = true
	end
end
