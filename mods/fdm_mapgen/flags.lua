-- LUALOCALS < ---------------------------------------------------------
local minetest
    = minetest
-- LUALOCALS > ---------------------------------------------------------

-- The "flat" mapgen already provides most of the functionality we want,
-- we just need to make sure that it's TRULY flat and that no additional
-- feature flags are enabled causing surface imperfections.

minetest.set_mapgen_setting("flags", "nocaves,nodungeons", true)
minetest.set_mapgen_setting("mgflat_spflags", "nolakes,nohills", true)
