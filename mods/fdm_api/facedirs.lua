-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, rawget, rawset, vector
    = minetest, pairs, rawget, rawset, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local api = rawget(_G, modname) or {}
rawset(_G, modname, api)

api.dirs = {
	u = vector.new(0, 1, 0),
	d = vector.new(0, -1, 0),
	e = vector.new(1, 0, 0),
	w = vector.new(-1, 0, 0),
	n = vector.new(0, 0, 1),
	s = vector.new(0, 0, -1)
}

api.facedirs = {
	{"u", "w"},
	{"u", "n"},
	{"u", "e"},
	{"n", "u"},
	{"n", "w"},
	{"n", "d"},
	{"n", "e"},
	{"s", "d"},
	{"s", "w"},
	{"s", "u"},
	{"s", "e"},
	{"e", "s"},
	{"e", "u"},
	{"e", "n"},
	{"e", "d"},
	{"w", "s"},
	{"w", "d"},
	{"w", "n"},
	{"w", "u"},
	{"d", "s"},
	{"d", "e"},
	{"d", "n"},
	{"d", "w"},
	[0] = {"u", "s"}
}

-- Facedir vectors:
-- t = top
-- b = bottom
-- f = front
-- k = back
-- l = left
-- r = right

for k, t in pairs(api.facedirs) do
	t.id = k
	t.t = api.dirs[t[1]]
	t.f = api.dirs[t[2]]
	t[2] = nil
	t[1] = nil
	t.l = vector.cross(t.t, t.f)
	t.r = vector.multiply(t.l, -1)
	t.b = vector.multiply(t.t, -1)
	t.k = vector.multiply(t.f, -1)
end
