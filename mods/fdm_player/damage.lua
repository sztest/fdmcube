-- LUALOCALS < ---------------------------------------------------------
local minetest
    = minetest
-- LUALOCALS > ---------------------------------------------------------

-- This is a pure creative-mode game type, so effectively disable
-- damage even if it's accidentally enabled in the settings, and
-- hide all damage-related features possible.

minetest.register_on_joinplayer(function(player)
		player:set_armor_groups({immortal = 1})
		player:hud_set_flags({
				healthbar = false,
				breathbar = false,
			})
	end)

minetest.register_on_player_hpchange(function() return 0 end, true)
