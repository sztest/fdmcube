-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, rawget, rawset
    = minetest, pairs, rawget, rawset
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local api = rawget(_G, modname) or {}
rawset(_G, modname, api)

-- Allow mods to register on_nodeupdate hooks to be called whenever
-- a node changes.

local registered = {}
api.registered_on_nodeupdates = registered
api.register_on_nodeupdate = function(func)
	registered[#registered + 1] = func
end

-- Helper method to run all node update handlers for a position.

local hash = minetest.hash_node_position
local mask = {}
local function notify_node_update(pos, node, ...)
	local phash = hash(pos)
	if mask[phash] then return ... end
	mask[phash] = true
	node = node or minetest.get_node(pos)
	for i = 1, #registered do
		(registered[i])(pos, node)
	end
	mask[phash] = nil
	return ...
end
api.notify_node_update = notify_node_update

-- Hook all API methods that can change nodes to run updates.
-- This captures everything except VoxelManip and engine fluid
-- transform; the latter is possible too, but not necessary since
-- this game has no fluids. For the VoxelManip case, we just
-- have to handle the possibility that this is leaky and patrol
-- for missed nodes with ABMs.

for fn, param in pairs({
		set_node = true,
		add_node = true,
		remove_node = false,
		swap_node = true,
		dig_node = false,
		place_node = true,
		add_node_level = false
	}) do
	local func = minetest[fn]
	minetest[fn] = function(pos, pn, ...)
		return notify_node_update(pos, param and pn, func(pos, pn, ...))
	end
end
