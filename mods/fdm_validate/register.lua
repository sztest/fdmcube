-- LUALOCALS < ---------------------------------------------------------
local error, minetest, pairs, rawget, rawset, string
    = error, minetest, pairs, rawget, rawset, string
local string_format
    = string.format
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local api = rawget(_G, modname) or {}
rawset(_G, modname, api)

function api.register_validation(name, validate, source)
	local def = minetest.registered_nodes[name]
	if not def then return error(string_format("node %q not found", name)) end

	-- Skip if we've already finished registration (alternatives cycle).
	if def.fdm_validate == validate and def.fdm_source == source then return end

	-- Alternatives must exist to be able to do validation.
	if validate and not (def.fdm_alternative_good or def.fdm_alternative_bad) then
		return error(string_format("node %q cannot validate without alternatives", name))
	end

	-- If the node is meant to be validated, then also register it
	-- with the necessary group to be picked up by ABMs/LBMs to check
	-- validation after the fact.
	local groups = {}
	for k, v in pairs(def.groups or {}) do groups[k] = v end
	groups[modname] = validate and 1 or 0

	-- If this node has a "good" alternative then it must be the "bad" version
	-- so register a group for the warning HUD.
	if def.fdm_alternative_good then groups[modname .. "_bad"] = 1 end

	-- Apply item overrides.
	minetest.override_item(name, {
			fdm_validate = validate,
			fdm_source = source,
			groups = groups
		})

	-- Apply same validation routines to alternatives.
	if def.fdm_alternative_bad then api.register_validation(def.fdm_alternative_bad, validate, source) end
	if def.fdm_alternative_good then api.register_validation(def.fdm_alternative_good, validate, source) end
end
