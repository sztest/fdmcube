-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, next, pairs, vector
    = ipairs, minetest, next, pairs, vector
-- LUALOCALS > ---------------------------------------------------------

-- An "on_fdm_zapper(pos, node, player)" hook that nodes can implement
-- to receive an alternative dig/destroy signal that has a cooldown
-- to prevent instantly hitting nodes behind the destroyed one, but
-- also hits the first node instantly for crisper feedback.

local cooldowns = {}
local function checkzap(player)
	local pname = player:get_player_name()
	if not player:get_player_control().LMB then
		cooldowns[pname] = nil
		return
	end
	if cooldowns[pname] then return end
	if not minetest.check_player_privs(pname, "interact") then return end
	local ppos = player:get_pos()
	ppos.y = ppos.y + player:get_properties().eye_height
	local lookdir = player:get_look_dir()
	local range = player:get_wielded_item():get_definition().range or 4
	local target = vector.add(ppos, vector.multiply(lookdir, range))
	for pt in minetest.raycast(ppos, target, false, false) do
		if pt.type == "node" then
			local node = minetest.get_node(pt.under)
			local def = minetest.registered_nodes[node.name]
			if def and def.on_fdm_zapper then
				def.on_fdm_zapper(pt.under, node, player)
				cooldowns[pname] = 0.25
				return
			end
		end
	end
end
minetest.register_globalstep(function(dtime)
		if next(cooldowns) then
			local newcd = {}
			for k, v in pairs(cooldowns) do
				if v > dtime then
					newcd[k] = v - dtime
				end
			end
			cooldowns = newcd
		end
		for _, player in ipairs(minetest.get_connected_players()) do
			checkzap(player)
		end
	end)
