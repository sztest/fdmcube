-- LUALOCALS < ---------------------------------------------------------
local minetest, rawget, rawset, vector
    = minetest, rawget, rawset, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local api = rawget(_G, modname) or {}
rawset(_G, modname, api)

-- Maximum total allowed length (in nodes) for FDM bridging. This would
-- need to be fine-tuned for print material, settings, and scale to be
-- optimal in practice, so here it only really functions as a sort of
-- loose heuristic.
local bridgelength = 16

-- Helper to check for support from below.
local function suppbelow(pos)
	return api.source_check(vector.add(pos, vector.new(0, -1, 0)), nil, pos)
end

-- Check if there's a valid bridge in one direction: a series of nodes in one
-- direction that ends with one supported from below within the specified length.
local function checkbridgecore(pos, dir, length)
	if not api.source_check(pos, nil, vector.add(pos, vector.multiply(dir, -1))) then return end
	if suppbelow(pos) then return length end
	if length < 1 then return end
	return checkbridgecore(vector.add(pos, dir), dir, length - 1)
end

-- Check for a valid bridge bidirectionally: there is a valid bridging support
-- in one direction, and another one in the opposite direction, with total length
-- within the maximum limit.
local function checkbridge(pos, dir, length)
	local left = checkbridgecore(pos, dir, length)
	if not left then return end
	return checkbridgecore(pos, vector.multiply(dir, -1), left)
end

api.register_validation("fdm_material:basic",
	function(pos)
		-- Basic nodes can be supported directly from below.
		if suppbelow(pos) then return true end

		-- Bridges can run along either axis. We do not attempt to
		-- support diagonal bridges currently.
		local xb = checkbridge(pos, vector.new(1, 0, 0), bridgelength)
		local zb = checkbridge(pos, vector.new(0, 0, 1), bridgelength)

		-- Bridges can be either horizontal or vertical, but may not
		-- cross (which would cause perimeters to make a turn without
		-- support from below, losing bridge tension). Support is
		-- required at a crossing point.
		return (xb or zb) and not (xb and zb)
	end,
	function() return true end)
