# FDM Cube

Cubey-style 3D FDM printing model designer for Minetest

- [ContentDB](https://content.minetest.net/packages/Warr1024/fdmcube/)
- [GitLab](https://gitlab.com/sztest/fdmcube)
- [Discord](https://discord.gg/KMbCjAHBTs)

## Concept

- Design 3D printable models inside Minetest
- Multiplayer cooperative/collaborative building experience

## Planned Features

- Focus on shapes rather than textures
	- For simple FDM prints without color or material changers
	- Fit all materials in hotbar, no need for inventory
- In-game structural analysis
	- Detect floating unsupported structures
	- Detect bad overhangs
	- Detect bad/risky bridging
	- Visual warnings
- Easy navigation
	- Waypoints, teleporting