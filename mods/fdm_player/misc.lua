-- LUALOCALS < ---------------------------------------------------------
local minetest
    = minetest
-- LUALOCALS > ---------------------------------------------------------

minetest.register_on_joinplayer(function(player)
		player:set_properties({
				-- Players can climb up whole blocks when not flying.
				stepheight = 1.05,

				-- Set a reasonable zoom FOV for screenshot photography.
				zoom_fov = 60,
			})

		player:set_physics_override({
				-- MTG walking speed is too tedious for large model
				-- building; players who want more precision can
				-- toggle off fast mode.
				speed = 2,

				-- Compensate for perception of shorter player when
				-- not flying by making player jump much higher.
				jump = 2
			})

		-- Enable players to use the minimap, but disable the "caves radar"
		-- feature, as everything will be built above the print bed and be
		-- visible on the normal view, and the extra modes to cycle through
		-- when setting zoom level are just an annoyance.
		player:hud_set_flags({
				minimap = true,
				minimap_radar = false,
			})
	end)
