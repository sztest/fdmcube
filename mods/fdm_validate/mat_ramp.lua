-- LUALOCALS < ---------------------------------------------------------
local fdm_api, ipairs, minetest, rawget, rawset, vector
    = fdm_api, ipairs, minetest, rawget, rawset, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local api = rawget(_G, modname) or {}
rawset(_G, modname, api)

api.register_validation("fdm_material:ramp",
	function(pos, node)
		local facedir = fdm_api.facedirs[node.param2]

		-- XXX: SIMPLIFY: disallow "wall" placed ramps entirely
		-- for the time being; these are only intended to be used as
		-- overhangs, and only allowed to be used floor-oriented for
		-- now (top contouring) because it happens to be simple to
		-- support.
		if facedir.b.y == 0 then return end

		-- Ramps with a flat bottom only need bottom support.
		-- XXX: SIMPLIFY: don't attempt to support bridging with
		-- non-full-cubes for now.
		-- Floor-placed ramps will always have their "bottom" vector
		-- downwards, we don't need to worry about cases where the "back"
		-- vector is the downwards one.
		if facedir.b == fdm_api.dirs.d then
			return api.source_check(vector.add(pos, facedir.b), nil, pos)
		end

		-- Celing placement case: must receive support on any "full" face
		-- that's not providing support upward.
		for _, dir in ipairs({facedir.b, facedir.k}) do
			if dir ~= fdm_api.dirs.u and not api.source_check(vector.add(pos, dir), nil, pos)
			then return end
		end
		-- Ceiling placement: the nodes we're reciving support from must
		-- be supported fully from below to have something for the bottom
		-- lip of our node to build from.
		if (not api.overhang_check(vector.add(pos, facedir.k)))
		then return end
		return true
	end,
	function(pos, node, from)
		-- Ramps only provide upward support if they have a full
		-- flat face in the direction in question.
		local fromdir = vector.subtract(from, pos)
		local facedir = fdm_api.facedirs[node.param2]
		if vector.equals(fromdir, facedir.b) or vector.equals(fromdir, facedir.k)
		then return true end
		if vector.equals(fromdir, facedir.l) or vector.equals(fromdir, facedir.r)
		then return false, vector.add(facedir.b, facedir.k) end
	end)
