-- LUALOCALS < ---------------------------------------------------------
local fdm_api, minetest, next, pairs, vector
    = fdm_api, minetest, next, pairs, vector
-- LUALOCALS > ---------------------------------------------------------

local hash = minetest.hash_node_position

-- Limit how much lag each step the bed re-filler can create.
local cycle_us = 1000000 * 0.2

-- Node name to refill bed with.
local bedname = "fdm_material:bed"

-- "Double queue" arrangement: one "next generation" queue that things
-- can be added to, and a "current generation" batch that we're working
-- through right now with a stable position pointer.
local queue = {}
local batch = {}
local bpos = 1

-- Attempt to heal the bed at a position. Do sanity checks, and if there
-- is missing bed, replace it, then queue all surrounding nodes for
-- recursive checking to fill entire holes larger than 1 node.
local function bedheal(pos)
	if pos.y > 8 then return end
	local node = minetest.get_node(pos)
	if node.name == bedname then return end
	node.name = bedname
	minetest.set_node(pos, node)
	for dx = -1, 1 do
		for dy = -1, 1 do
			for dz = -1, 1 do
				local p = vector.add(pos, vector.new(dx, dy, dz))
				queue[hash(p)] = p
			end
		end
	end
end

-- Process all queues in globalstep.
minetest.register_globalstep(function()
		-- Stop iterating when we hit a time limit and let the
		-- game run until the next cycle, so we don't lag the server
		-- to death filling arbitrary-sized holes.
		local maxtime = minetest.get_us_time() + cycle_us
		while minetest.get_us_time() < maxtime do
			if bpos > #batch then
				if next(queue) == nil then return end
				batch = {}
				bpos = 1
				for _, v in pairs(queue) do batch[#batch + 1] = v end
				queue = {}
			end
			bedheal(batch[bpos])
			bpos = bpos + 1
		end
	end)

-- Any time a detectable node update happens, check for bed healing
fdm_api.register_on_nodeupdate(function(pos)
		if pos.y <= 8 then queue[hash(pos)] = pos end
	end)

------------------------------------------------------------------------
-- Bed Conversion

-- If any "bed" nodes end up above the actual print bed (worldedit) then
-- convert them to "basic" material automatically.

local solidname = "fdm_material:basic"

minetest.register_abm({
		nodenames = {bedname},
		interval = 2,
		chance = 1,
		min_y = 9,
		action = function(pos)
			minetest.set_node(pos, {name = solidname})
		end
	})
