-- LUALOCALS < ---------------------------------------------------------
local include
    = include
-- LUALOCALS > ---------------------------------------------------------

include("misc")
include("autoprivs")
include("damage")
include("hand")
include("inventory")
include("sky")
include("zapper")
include("model")
