-- LUALOCALS < ---------------------------------------------------------
-- SKIP: include
local dofile, minetest, rawget, rawset, table
    = dofile, minetest, rawget, rawset, table
local table_concat, table_insert
    = table.concat, table.insert
-- LUALOCALS > ---------------------------------------------------------

-- Helper function to make it easier to break mods into multiple files
-- for easier code navigation, and load all those files easily in init.lua.

local include = rawget(_G, "include") or function(...)
	local parts = {...}
	table_insert(parts, 1, minetest.get_modpath(minetest.get_current_modname()))
	if parts[#parts]:sub(-4) ~= ".lua" then
		parts[#parts] = parts[#parts] .. ".lua"
	end
	return dofile(table_concat(parts, "/"))
end
rawset(_G, "include", include)

------------------------------------------------------------------------

include("facedirs")
include("nodeupdate")
